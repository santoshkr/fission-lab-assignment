# -*- coding: utf-8 -*-
"""
Created on Sat Apr  8 20:25:53 2017

@author: santosh
"""

def leastPrimeFactor(n):
    #table is a dictionary to store the least prime factor 
    #corresponding to a key 
    table=dict()    
    table['1'] = 1;
    
    for i in range(2,n+1):
        #if 'i' is not the key of table 
        #then i is prime, so, table['i']=i
        if(str(i) not in table):
            table[str(i)]= i
            j=2*i
            #all the multiples of i wound 
            #have same least prime factor i.e. i
            while(j<=n):
                if(str(j) not in table):
                    table[str(j)]= i
                j=j+i
    return table[str(n)]
    
#number of contests
tc=int(raw_input())
for i in range(tc):
    #n in number of questions in each contests
    n=int(raw_input())
    #x is number of problem solved by herbal
    x=leastPrimeFactor(n)
    print n-x, x