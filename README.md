This is a solution to an assignment.
Problem Statement:

Naren, and Herbal are best friends and also mutants. So, needless to say that both of them

have super powers. But Herbal has an additional super power, that is, being extremely good

at Mathematics. So, he keeps fooling Naren with his super-power all the time. But this time,

they have decided to form a team and compete in the prestigious ACM ICPC contest.

Naren, now knows that there are total N problems in a contest, and he cannot solve all of

them. He wants to do it fairly, and divide the problems equally with Herbal. Herbal, on the

other hand, wants to confuse Naren as much as possible, so he keeps a weird condition. The

condition is as following:

1. The number of problems in the problemset equals to N.

2. The number N will have some prime factors.

3. Herbal will solve X problems in the contest.

4. X denotes the minimum prime factor of N.

5. Naren solves the rest of the problems.

You've to find out how many problems Naren solved, and how many did Herbal solve.

Input Format:

The first line contains an integer TC, which denotes the number of contests their team has to

deal with. The next TC lines contain an integer N, denoting the number of problems in that

contest.

Output Format:

For every contest, you've to print two integers, separated by a space - the first one denoting

the number of problems solved by Herbal, and the second one shows number of problems

solved by Naren.
