# -*- coding: utf-8 -*-
"""
Created on Sat Apr  9 20:39:39 2017

@author: santosh
"""


'''
  A dynamic programming based approach
  for calculating least prime factor(lpf)

'''

#checks if j is divisible by any number in list l
#l contains list of prime number passed by leastPrimeFactorDP function
#returns true if not divisible
#else sets the least prime of that key to the number
#and returns false

def notDiv(table,j,l):
    for prime in l:
        if(j%prime ==0):
            table[str(j)]=prime
            return False
    return True
 

#calculates least prime factor of all the number from start to n
# and stores it in table 
#table is a dictionary to store the least prime factor  
  
def leastPrimeFactorDP(table,start, n):
    #if we already caculated lpf of n 
    #then return it
    if(str(n) in table):
        return table[str(n)]
    
    table['1']=1
    
    #unique_set stores set of prime numbers already in table
    #in sorted order
    unique_set= set(table.values())
    unique_set.remove(1)
    unique_set= sorted(unique_set, key=int)
    
    
    
    flag=1;
    for i in range(start,n+1):
        #if 'i' is not in table, we have to check 
        #if any prime from prime set divides i
        #if that happens then lpf of 'i' is that number
        #else i is a prime
        if(str(i) not in table):
            flag=1
            # l is a list of prime number who cannot divide i
            l=list()
            for prime in unique_set:
                if(i%prime ==0 ):
                    flag=0
                    k=prime
                    break
                l.append(prime)
            #if below condition is true 
            #then i is a prime
            if(flag):
                k=i
            #k is the lpf of 'i'
            if(str(i) not in table):
                table[str(i)]=k
            j=i+k
            #all the multiples of k will 
            #have same least prime factor(lpf) i.e. k 
            #if it is not divisible by any number in list l
            while(j<=n):
                if(str(j) not in table and notDiv(table,j,l)):
                    table[str(j)]= k
                j=j+k
    
    return table[str(n)]
    
#number of contests
tc=int(raw_input())
#table is a dictionary to store the least prime factor 
table=dict()
start=2
for i in range(tc):
    #n in number of questions in each contests
    n=int(raw_input())
    #x is number of problem solved by herbal
    x=leastPrimeFactor(table,start,n)
    print n-x, x

